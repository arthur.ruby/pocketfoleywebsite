# Pocket Foley Website

## Présentation
Ce projet est un site internet de présentation qui accompagne l'application mobile PocketFoley.

Il est développé en PHP avec le framework Symfony, le framework de style Bootstrap et s'appuie sur une base de données gérée par le SGBD MySQL.

## Fonctionnalités

#### Utilisateur lambda
Le site permet à un utilisateur quelconque (non-authentifié) de consulter différentes rubriques:
- Une liste des actualités concernant le développement de l'application, triées de la plus récente à la plus ancienne
- Une section "A propos" qui présente l'application en détail
- Une galerie qui affiche différentes captures de l'application mobile
- Une page de téléchargements qui explique comment se procurer l'application

#### Administrateur
Le site permet de s'authentifier en tant qu'administrateur.

Un administrateur a accès à une nouvelle page qui lui permet de poster une nouvelle actualité directement grâce à un formulaire.

## Screenshots

![page actualités](screenshots/actualites_deconnecte_large.png)
-
![page actualités mobile](screenshots/actualites_connecte__deroule_small.png)
-
![page à propos](screenshots/a_propos_large_deconnecte.png)
-
![page galerie](screenshots/galerie_deconnecte_large.png)
-
![page téléchargement](screenshots/telecharger_deconnecte_large.png)
-
![page login](screenshots/login_deconnecte_large.png)
-
![page nouvelle actu](screenshots/nouvelle_actualite_large.png)
-

## Licence
Le projet est sous licence MIT.
