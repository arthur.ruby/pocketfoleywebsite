<?php

namespace App\Controller;

use App\Entity\Actualite;
use App\Form\ActualiteType;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/modification", name="modification_")
 */
class ModificationController extends AbstractController
{
	/**
	 * @Route("/nouvelleactualite", name="actualite_new")
	 */
    public function create(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, Security $security)
    {
	    $this->denyAccessUnlessGranted('ROLE_ADMIN', '', 'Accès refusé: seul un administrateur est autorisé à voir cette page');

	    $actualite = new Actualite();
	    $actualiteForm = $this->createForm(ActualiteType::class, $actualite);

	    // récupération de la requête
	    // + injection des données du formulaire dans l'objet Article
	    $actualiteForm->handleRequest($request);
	    // on vérifie si le formulaire a été soumis
	    if ($actualiteForm->isSubmitted() && $actualiteForm->isValid())
	    {
		    $dateNow = new DateTime('now');
	    	$actualite->setDate($dateNow);
	    	$em->persist($actualite);
		    $em->flush();
		    return $this->redirectToRoute('main_home',
		    [
			    'title' => "Actualités",
		    ]);
	    }
	    return $this->render("actualite/index.html.twig",
		    [
			    'title' => "Nouvelle Actualité",
			    'actualiteForm' => $actualiteForm->createView()
		    ]);
    }
}
