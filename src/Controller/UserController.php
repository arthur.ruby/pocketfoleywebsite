<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserController extends AbstractController
{
//    /**
//     * @Route("/user", name="user")
//     */
    public function register(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, Security $security)
    {
//	    $this->denyAccessUnlessGranted('ROLE_ADMIN', '', 'Accès refusé: seul un administrateur est autorisé à voir cette page');
//	    return $this->render('user/index.html.twig', [
//            'controller_name' => 'UserController',
//        ]);
	    $user = new User();
	    $userForm = $this->createForm(UserType::class, $user);

	    // récupération de la requête
	    // + injection des données du formulaire dans l'objet Article
	    $userForm->handleRequest($request);
	    // on vérifie si le formulaire a été soumis
	    if ($userForm->isSubmitted() && $userForm->isValid())
	    {
		    $password = $encoder->encodePassword($user, $user->getPassword());
			$user->setPassword($password);
		    $em->persist($user);
		    $em->flush();
	    }
	    return $this->render("user/index.html.twig",
		    [
			    'title' => "Inscription",
			    'userForm' => $userForm->createView()
		    ]);
    }
}
