<?php

namespace App\Controller;

use App\Entity\Actualite;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main_home")
     */
    public function index( EntityManagerInterface $em)
    {
    	$actualiteRepo = $em->getRepository(Actualite::class);
		$actualites = $actualiteRepo->findBy([],['date' => 'DESC']);

    	return $this->render('main/index.html.twig', [
            'title' => 'Actualités',
		    'actualites' => $actualites
        ]);
    }

	/**
	 * @Route("/apropos", name="a_propos")
	 */
    public function aPropos()
    {
	    return $this->render('main/apropos.html.twig', [
		    'title' => 'A Propos',
	    ]);
    }

	/**
	 * @Route("/telecharger", name="telecharger")
	 */
	public function telecharger()
	{
		return $this->render('main/telecharger.html.twig', [
			'title' => 'Télécharger',
		]);
	}

	/**
	 * @Route("/galerie", name="galerie")
	 */
	public function galerie()
	{
		return $this->render('main/galerie.html.twig', [
			'title' => 'Galerie',
		]);
	}
}
