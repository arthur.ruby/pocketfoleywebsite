<?php

namespace App\Form;

use App\Entity\Actualite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActualiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
	            [
		            'label' => 'Titre',
		            'attr' =>
			            [
				            'placeholder' => "Titre de l'actualité..."
			            ]
	            ])
            ->add('date', HiddenType::class)
            ->add('description', TextareaType::class,
	            [
		            'label' => "Description",
		            'attr' =>
			            [
				            'placeholder' => "Contenu de l'actualité..."
			            ]
	            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Actualite::class,
        ]);
    }
}
